import NXTfusion.NXTfusion as NX
import NXTfusion.DataMatrix as DM 
import NXTfusion.NXLosses as L
from NXTfusion.NXmodels import NXmodelProto
from NXTfusion.NXmultiRelSide import NNwrapper
from NXTfusion.NXFeaturesConstruction import buildPytorchFeats
import numpy as np
from scipy.io import mmread
import torch as t
DEVICE = "cpu:0" #change here to cuda:X if you want GPU acceleration
IGNORE_INDEX = -9999

#BEFORE RUNNING PLEASE DOWNLOAD THE DATASETS:
#wget http://homes.esat.kuleuven.be/~jsimm/chembl-IC50-346targets.mm
#wget http://homes.esat.kuleuven.be/~jsimm/chembl-IC50-compound-feat.mm


def main(args):
	print("Start")
	#load the target matrix and transpose it to have prot, drugs as axes
	ic50 = mmread("chembl-IC50-346targets.mm").transpose()
	shape = ic50.shape
	print(shape)
	#read the side information (features)
	#requires 12Gb of ram, so we propose a smaller (randomly generated) alternative
	#(Sparse support for side information is currentyl missing)
	ecfp = mmread("chembl-IC50-compound-feat.mm")
	ecfp = np.random.rand(ecfp.shape[0], 50)
	print(ecfp.shape)
	#defining the entities involved in the relation
	protEnt = NX.Entity("proteins", list(range(0,shape[0])), np.int16)
	drugEnt = NX.Entity("compounds", list(range(0,shape[1])), np.int16)
	#transforming the matrix into a NX format
	ic50DrugMat = DM.DataMatrix("ic50", protEnt, drugEnt, ic50)
	#transform the side info in a NX format
	ecfpSideMat = DM.SideInfo("drugSide", drugEnt, ecfp)
	#defining the relation
	protDrugLoss = L.LossWrapper(t.nn.MSELoss(), type="regression", ignore_index = IGNORE_INDEX)
	#defining the meta-relation that wraps all the protein-drug relations
	protDrugRel = NX.MetaRelation("prot-drug", protEnt, drugEnt, None, ecfpSideMat)
	#adding the actual matrix/relation to the meta-relation
	protDrugRel.append(NX.Relation("drugInteraction", protEnt, drugEnt, ic50DrugMat, "regression", protDrugLoss, relationWeight=1))
	#finally we create the ER graph obect hosting this relation
	ERgraph = NX.ERgraph([protDrugRel])
	#we build the pytorch NN model used to make inference over the ERgraph
	model = example1Model(ERgraph, "mod1")
	#to allow the multi-task training, we assing the model to the standard wrapper
	wrapper = NNwrapper(model, dev = DEVICE, ignore_index = IGNORE_INDEX)
	#the wrapper responds to the scikit-learn like methods fit, predict
	wrapper.fit(ERgraph, epochs=5)
	#after training, in order to predict certain entries of certain matrices/relations
	#we build the corresponding input vectors
	
	X, Y, corresp = buildPytorchFeats(ic50DrugMat)
	#we use the predict function of the wrapper to predict certain entriex (X) of the target relation "drugInteraction" withing the corresponding MetaRelation "prot-drug" of the given ERgraph.
	#in this case, the MetaRelation "prot-drug" contains only one relation, because we are showing the simplest, matrix factorization-like model, but it could contain multiple relations, making it a tensor factorization problem.
	Yp = wrapper.predict(ERgraph, X, "prot-drug", "drugInteraction", None, ecfpSideMat)
	#predictions are provided as output. To show that the model was able to learn the random values, we print the Mean Squared Error. MSE < 0.1 with epochs > 50.
	print("Final MSE: ", (np.sum((np.array(Yp) - np.array(Y))**2))/float(len(Yp)))

	#we do the same but taking as input the coo_matrix instead
	X, Y, corresp = buildPytorchFeats(ic50, protEnt, drugEnt)
	Yp = wrapper.predict(ERgraph, X, "prot-drug", "drugInteraction", None, ecfpSideMat)
	print("Final MSE: ", (np.sum((np.array(Yp) - np.array(Y))**2))/float(len(Yp)))

	ecfpFake = np.ones((ecfp.shape[0], 50))*1000
	ecfpSideMatFake = DM.SideInfo("drugSide", drugEnt, ecfpFake)
	Yp = wrapper.predict(ERgraph, X, "prot-drug", "drugInteraction", None, ecfpSideMatFake)
	print("Final MSE: ", (np.sum((np.array(Yp) - np.array(Y))**2))/float(len(Yp)))


class example1Model(NXmodelProto):
	def __init__(self, ERG, name):
		super(example1Model, self).__init__()
		self.name = name
		##########DEFINE NN HERE##############
		protEmbLen = ERG["prot-drug"]["lenDomain1"]
		drugEmbLen = ERG["prot-drug"]["lenDomain2"]
		PROT_LATENT_SIZE = 10
		DRUG_LATENT_SIZE = 20
		ACTIVATION = t.nn.Tanh
		self.protEmb = t.nn.Embedding(protEmbLen, PROT_LATENT_SIZE)
		self.protHid = t.nn.Sequential(t.nn.Linear(PROT_LATENT_SIZE, 10), t.nn.LayerNorm(10), ACTIVATION())
		self.sideHid2 = t.nn.Sequential(t.nn.Linear(50, 20), t.nn.LayerNorm(20), ACTIVATION())
		self.sideMix2 = t.nn.Bilinear(DRUG_LATENT_SIZE, 20, 20)	
		self.drugEmb = t.nn.Embedding(drugEmbLen, DRUG_LATENT_SIZE)
		self.drugHid = t.nn.Sequential(t.nn.Linear(20, 20), t.nn.LayerNorm(20), ACTIVATION())
		self.biProtDrug = t.nn.Bilinear(10, 20, 10)
		self.outProtDrug = t.nn.Sequential( t.nn.LayerNorm(10), ACTIVATION(), t.nn.Dropout(0.1), t.nn.Linear(10,1))
		self.apply(self.init_weights)

	def forward(self, relName, i1, i2, s1, s2):

		if relName == "prot-drug":
			v = self.drugEmb(i2)
			v = self.sideMix2(v, self.sideHid2(s2))
			u = self.protEmb(i1)
			u = self.protHid(u).squeeze()
			v = self.drugHid(v).squeeze()
			o = self.biProtDrug(u, v)
			o = self.outProtDrug(o)
			return o

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))

