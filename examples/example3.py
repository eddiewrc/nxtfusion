import NXTfusion.NXTfusion as NX
import NXTfusion.DataMatrix as DM 
import NXTfusion.NXLosses as L
from NXTfusion.NXmodels import NXmodelProto
from NXTfusion.NXmultiRelSide import NNwrapper
from NXTfusion.NXFeaturesConstruction import buildPytorchFeats
import numpy as np
from scipy.io import mmread
import torch as t
DEVICE = "cpu:0" #change here to cuda:X if you want GPU acceleration
IGNORE_INDEX = -9999

def main(args):
	print("Start")
	#defining the entities involved in the relation
	protEnt = NX.Entity("proteins", list(range(0,100)), np.int16)
	drugEnt = NX.Entity("compounds", list(range(0,1000)), np.int16)
	domainEnt = NX.Entity("protein", list(range(0,700)), np.int16)

	#defining the matrices describing the relations
	protDrugMat = np.random.rand(100, 1000)
	protDomainMat = np.random.randint(2, size=(100, 700))
	#transforming the matrices into a NX format
	protDrugMat = DM.DataMatrix("protDrugMatrix", protEnt, drugEnt, protDrugMat)
	protDomainMat = DM.DataMatrix("protDomainMatrix", protEnt, domainEnt, protDomainMat)
	#defining the relation losses
	protDrugLoss = L.LossWrapper(t.nn.MSELoss(), type="regression", ignore_index = IGNORE_INDEX)
	protDomainLoss = L.LossWrapper(t.nn.BCEWithLogitsLoss(), type="binary", ignore_index = IGNORE_INDEX)
	#defining the meta-relation that wraps all the protein-drug relations
	protDrugRel = NX.MetaRelation("prot-drug", protEnt, drugEnt, None, None)
	#adding the actual matrix/relation to the meta-relation
	protDrugRel.append(NX.Relation("drugInteraction", protEnt, drugEnt, protDrugMat, "regression", protDrugLoss, relationWeight=1))

	protDomainRel = NX.MetaRelation("prot-domain", protEnt, domainEnt, None, None)
	protDomainRel.append(NX.Relation("pfamDomains", protEnt, domainEnt, protDomainMat, "binary", protDomainLoss, relationWeight=1))
	#finally we create the ER graph object by adding BOTH relations!
	ERgraph = NX.ERgraph([protDrugRel, protDomainRel])
	#we build the pytorch NN model used to make inference over the ERgraph
	model = example3Model(ERgraph, "mod3")
	#to allow the multi-task training, we assing the model to the standard wrapper
	wrapper = NNwrapper(model, dev = DEVICE, ignore_index = IGNORE_INDEX)
	#the wrapper responds to the scikit-learn like methods fit, predict
	wrapper.fit(ERgraph, epochs=5)
	#after training, in order to predict certain entries of certain matrices/relations
	#we build the corresponding input vectors
	X, Y, corresp = buildPytorchFeats(protDrugMat)
	#we use the predict function of the wrapper to predict certain entriex (X) of the target relation "drugInteraction" withing the corresponding MetaRelation "prot-drug" of the given ERgraph.
	#in this case, the MetaRelation "prot-drug" contains only one relation, because we are showing the simplest, matrix factorization-like model, but it could contain multiple relations, making it a tensor factorization problem.
	Yp = wrapper.predict(ERgraph, X, "prot-drug", "drugInteraction", None, None)
	#predictions are provided as output. To show that the model was able to learn the random values, we print the Mean Squared Error. MSE < 0.1 with epochs > 50.
	print("Final MSE: ", (np.sum((np.array(Yp) - np.array(Y))**2))/float(len(Yp)))

	#let's predict also the prot-domain relation!
	X, Y, corresp = buildPytorchFeats(protDomainMat)
	Yp = wrapper.predict(ERgraph, X, "prot-domain", "pfamDomains", None, None)
	print("Final MSE: ", (np.sum((np.array(Yp) - np.array(Y))**2))/float(len(Yp)))


class example3Model(NXmodelProto):
	def __init__(self, ERG, name):
		super(example3Model, self).__init__()
		self.name = name
		##########DEFINE NN HERE##############
		protEmbLen = ERG["prot-drug"]["lenDomain1"]
		drugEmbLen = ERG["prot-drug"]["lenDomain2"]
		domainEmbLen = ERG["prot-domain"]["lenDomain2"]
		PROT_LATENT_SIZE = 10
		DOMAIN_LATENT_SIZE = 10
		DRUG_LATENT_SIZE = 20
		ACTIVATION = t.nn.Tanh
		self.protEmb = t.nn.Embedding(protEmbLen, PROT_LATENT_SIZE)
		self.protHid = t.nn.Sequential(t.nn.Linear(PROT_LATENT_SIZE, 10), t.nn.LayerNorm(10), ACTIVATION())
		
		self.drugEmb = t.nn.Embedding(drugEmbLen, DRUG_LATENT_SIZE)
		self.drugHid = t.nn.Sequential(t.nn.Linear(DRUG_LATENT_SIZE, 20), t.nn.LayerNorm(20), ACTIVATION())
		self.biProtDrug = t.nn.Bilinear(10, 20, 10)
		self.outProtDrug = t.nn.Sequential( t.nn.LayerNorm(10), ACTIVATION(), t.nn.Dropout(0.1), t.nn.Linear(10,1))

		self.domainEmb = t.nn.Embedding(domainEmbLen, DOMAIN_LATENT_SIZE)
		self.domainHid = t.nn.Sequential(t.nn.Linear(DOMAIN_LATENT_SIZE, 20), t.nn.LayerNorm(20), ACTIVATION())
		self.biProtDomain = t.nn.Bilinear(10, 20, 10)
		self.outProtDomain = t.nn.Sequential( t.nn.LayerNorm(10), ACTIVATION(), t.nn.Dropout(0.1), t.nn.Linear(10,1))
	
		self.apply(self.init_weights)

	def forward(self, relName, i1, i2, s1=None, s2=None):
		if relName == "prot-drug":
			u = self.protEmb(i1)
			v = self.drugEmb(i2)
			u = self.protHid(u).squeeze()
			v = self.drugHid(v).squeeze()
			o = self.biProtDrug(u, v)
			o = self.outProtDrug(o)
		if relName == "prot-domain":
			u = self.protEmb(i1)
			v = self.domainEmb(i2)
			u = self.protHid(u).squeeze()
			v = self.domainHid(v).squeeze()
			o = self.biProtDomain(u, v)
			o = self.outProtDomain(o)

		return o

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))

