import NXTfusion.NXTfusion as NX
import NXTfusion.DataMatrix as DM 
import NXTfusion.NXLosses as L
from NXTfusion.NXmodels import NXmodelProto
from NXTfusion.NXmultiRelSide import NNwrapper
from NXTfusion.NXFeaturesConstruction import buildPytorchFeats
import numpy as np
from scipy.io import mmread
import torch as t
DEVICE = "cpu:0" #change here to cuda:X if you want GPU acceleration
IGNORE_INDEX = -9999

def main(args):
	print("Start")
	#defining the entities involved in the relation
	protEnt = NX.Entity("proteins", list(range(0,100)), np.int16)
	drugEnt = NX.Entity("compounds", list(range(0,1000)), np.int16)
	#defining the matrices that will be involved in the tensor factorization
	#they repreent 3 (different) relations between the Protein, Drug entities
	protDrugMat1 = np.random.rand(100, 1000)
	protDrugMat2 = np.random.rand(100, 1000)
	protDrugMat3 = np.random.rand(100, 1000)
	#transforming the matrices into a NX format
	protDrugMat1 = DM.DataMatrix("protDrugMatrix1", protEnt, drugEnt, protDrugMat1)
	protDrugMat2 = DM.DataMatrix("protDrugMatrix2", protEnt, drugEnt, protDrugMat2)
	protDrugMat3 = DM.DataMatrix("protDrugMatrix3", protEnt, drugEnt, protDrugMat3)
	#defining the relation losses. We specify a different loss for each matrix, just to show how it's done.
	protDrugLoss1 = L.LossWrapper(t.nn.MSELoss(), type="regression", ignore_index = IGNORE_INDEX)
	protDrugLoss2 = L.LossWrapper(t.nn.L1Loss(), type="regression", ignore_index = IGNORE_INDEX)
	protDrugLoss3 = L.LossWrapper(t.nn.SmoothL1Loss(), type="regression", ignore_index = IGNORE_INDEX)
	#defining the meta-relation that wraps all the protein-drug relations
	protDrugRel = NX.MetaRelation("prot-drug", protEnt, drugEnt, None, None)
	#adding the actual matrix/relation to the meta-relation
	protDrugRel.append(NX.Relation("drugInteraction1", protEnt, drugEnt, protDrugMat1, "regression", protDrugLoss1, relationWeight=1))
	protDrugRel.append(NX.Relation("drugInteraction2", protEnt, drugEnt, protDrugMat2, "regression", protDrugLoss2, relationWeight=1))
	protDrugRel.append(NX.Relation("drugInteraction3", protEnt, drugEnt, protDrugMat3, "regression", protDrugLoss3, relationWeight=1))
	#finally we create the ER graph obect hosting this MetaRelation, which contains 3 relations
	ERgraph = NX.ERgraph([protDrugRel])
	#we build the pytorch NN model used to make inference over the ERgraph
	model = example2Model(ERgraph, "mod2")
	#to allow the multi-task training, we assing the model to the standard wrapper
	wrapper = NNwrapper(model, dev = DEVICE, ignore_index = IGNORE_INDEX)
	#the wrapper responds to the scikit-learn like methods fit, predict
	wrapper.fit(ERgraph, epochs=5)
	#after training, in order to predict certain entries of certain matrices/relations
	#we build the corresponding input vectors
	X, Y, corresp = buildPytorchFeats(protDrugMat1)
	#we use the predict function of the wrapper to predict certain entries (X) of the target relation "drugInteraction1" within the corresponding MetaRelation "prot-drug" of the given ERgraph.
	#we thus specify in the .predict() function 1-the ERgraph on which the model has been trained, 2-the positions in the matrix/relation we are interested in (X), 3- the MetaRelation we are interested in ("prot-drug") and 4-the Relation in the target MetaRelation we want to predict "drugInteraction1". In this case, the MetaRelation "prot-drug" contains 3 relations between Drug and Proteins entity, but we predict one Relation at a time, even if they are learnt jointly.
	Yp = wrapper.predict(ERgraph, X, "prot-drug", "drugInteraction1", None, None)
	#predictions are provided as output. To show that the model was able to learn the random values, we print the Mean Squared Error. MSE < 0.1 with epochs > 50.
	print("Final MSE: ", (np.sum((np.array(Yp) - np.array(Y))**2))/float(len(Yp)))

	X, Y, corresp = buildPytorchFeats(protDrugMat2)
	Yp1 = wrapper.predict(ERgraph, X, "prot-drug", "drugInteraction2", None, None)
	print("Final MSE: ", (np.sum((np.array(Yp) - np.array(Y))**2))/float(len(Yp)))

	X, Y, corresp = buildPytorchFeats(protDrugMat3)
	Yp1 = wrapper.predict(ERgraph, X, "prot-drug", "drugInteraction3", None, None)
	print("Final MSE: ", (np.sum((np.array(Yp) - np.array(Y))**2))/float(len(Yp)))

class example2Model(NXmodelProto):
	def __init__(self, ERG, name):
		super(example2Model, self).__init__() #tensor factorization
		self.name = name
		##########DEFINE NN HERE##############
		protEmbLen = ERG["prot-drug"]["lenDomain1"]
		drugEmbLen = ERG["prot-drug"]["lenDomain2"]
		PROT_LATENT_SIZE = 20
		DRUG_LATENT_SIZE = 25
		ACTIVATION = t.nn.Tanh
		self.protEmb = t.nn.Embedding(protEmbLen, PROT_LATENT_SIZE)
		self.protHid = t.nn.Sequential(t.nn.Linear(PROT_LATENT_SIZE, 10), t.nn.LayerNorm(10), ACTIVATION())
		
		self.drugEmb = t.nn.Embedding(drugEmbLen, DRUG_LATENT_SIZE)
		self.drugHid = t.nn.Sequential(t.nn.Linear(DRUG_LATENT_SIZE, 20), t.nn.LayerNorm(20), ACTIVATION())
		self.biProtDrug = t.nn.Bilinear(10, 20, 10)
		self.outProtDrug = t.nn.Sequential( t.nn.LayerNorm(10), ACTIVATION(), t.nn.Dropout(0.1), t.nn.Linear(10,3))#the 3 output neurons are responsible for reconstructing the 3 matrices
		self.apply(self.init_weights)

	def forward(self, relName, i1, i2, s1=None, s2=None):
		if relName == "prot-drug":
			u = self.protEmb(i1)
			v = self.drugEmb(i2)
			u = self.protHid(u).squeeze()
			v = self.drugHid(v).squeeze()
			o = self.biProtDrug(u, v)
			o = self.outProtDrug(o)
			return o

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))

