.. NXTfusion documentation master file, created by
   sphinx-quickstart on Fri Feb 12 14:59:24 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to NXTfusion's documentation!
=====================================

NXTfusion is a pytorch-based library for non-linear data fusion over Entity-Relation graph.

.. toctree::
   :maxdepth: 2
  
   datafusion
   Install
   quickstart
   examples/index
   buildModel
   modules
   contact


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
