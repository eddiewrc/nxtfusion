Installation guide
==================

How do I set it up?
^^^^^^^^^^^^^^^^^^^
You can install NXTfusion either from the bitbucket (https://bitbucket.org/eddiewrc/nxtfusion/src/master/) repo or from our pypi package (https://pypi.org/project/nxtfusion/). 

In either ways, we recommend to follow these steps:

First, set up a dedicated conda environment, to avoid problems with existing softwares.

- Download and install miniconda from https://docs.conda.io/en/latest/miniconda.html
- Create a new conda environment by typing: ``conda create -n nxtfusion -python=3.6``

Enter the environment by typing: ``conda activate nxtfusion``

Install from git repo
^^^^^^^^^^^^^^^^^^^^^
If you want to install NXTfusion from this repo, you need to install the dependencies first.

Install ``pytorch >= 1.0`` with the command: ``conda install pytorch -c pytorch`` or refer to ``pytorch`` website https://pytorch.org
Install the remaining requirements with the command: ``conda install scipy numpy multipledispatch``
You can remove this environment at any time by typing: ``conda remove -n nxtfusion --all``

Install from pip
^^^^^^^^^^^^^^^^
If you want to install it via Pypi, just add pip to your conda environment: ``conda install pip``

and then just type: ``pip install nxtfusion``

and all the required dependencies will be installed automatically. Congrats, you're ready to rock!

Install from conda
^^^^^^^^^^^^^^^^^^

TODO
