How to cite
===========

If you find this library useful, please cite: https://doi.org/10.1093/bioinformatics/btab092.

About us
========

This library has been developed during my stay at KU Leuven (ESAT-STADIUS), funded by KU Leuven and FWO grants.


Disclaimer
==========

I did my best effort to make this library available to anyone, but bugs might be present.
Should you experience problems in using or installing it, or just to share any comment, please contact daniele [dot] raimondi [At] kuleuven [dOt] be.

Please note that: 

THIS SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.



