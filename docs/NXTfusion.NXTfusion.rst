NXTfusion.NXTfusion module
==========================

.. autoclass:: NXTfusion.NXTfusion.Entity
   :members:
   :show-inheritance:

      	.. py:method:: __getitem__(self, x:str)

	   Method that	returns the numeric value internally associated to each object in the Entity class.
			
	   :param str x: String name of a specific object in the Entity.

	   :return: primary key int

      	.. py:method:: __getitem__(self, x:int)

	   Method that	returns the str name of the object with primary key x.
			
	   :param int x: Primary key (unique id) of an object in the domain represented by the Entity.

	   :return: name of the object : str

.. autoclass:: NXTfusion.NXTfusion.Relation
   :members:
   :undoc-members:
   :show-inheritance:

.. autoclass:: NXTfusion.NXTfusion.MetaRelation
   :members:
   :undoc-members:
   :show-inheritance:

         .. py:method:: __getitem__(self, x:str)

	   Getitem method that searches by Relation.name
			
	   :param str x: The name of a Relation in this MetaRelation

	   :return: The target Relation or None

        .. py:method:: __getitem__(self, x:int)

	   Getitem method that searches by position of the target Relation in the tensor/MetaRelation.
			
	   :param str x: The position (index) of a Relation in this MetaRelation

	   :return: The target Relation or None

.. autoclass:: NXTfusion.NXTfusion.ERgraph
   :members:
   :undoc-members:
   :show-inheritance:

        .. py:method:: __contains__(self, x:int)

	   Function that determines whether a specific MetaRelation object is present in the graph.
			
	   :param MetaRelation x: A MetaRelation object.

	   :return: bool (Is x present?)

        .. py:method:: __contains__(self, x:str)

	   Function that determines whether a specific MetaRelation.name is present in the graph.
			
	   :param str x: A MetaRelation str name.

	   :return: bool (Is x present?)

