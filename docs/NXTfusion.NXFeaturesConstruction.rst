NXTfusion.NXFeaturesConstruction module
=======================================

.. automodule:: NXTfusion.NXFeaturesConstruction
      :members:
      :show-inheritance:

.. py:function:: buildPytorchFeats(data:numpy.ndarray, domain1:Entity, domain2:Entity, side1 = None, side2 = None)-> (list, list, list)

   This function is used to produce the input for the NNwrapper.predict() method, at prediction time. It produces the inputs necessary to predict the output for certain cells (or the entire matrix) for a given relation.

   This version of the method takes as prediction target a (dense) numpy matrix.

   :param numpy.ndarray data: Numpy matrix representing the target. This form of the method is more useful when the entire matrix needs to be predicted. The actual values in the matrix are provided as "labels" in output, but are ignored during prediction.
   :param Entity domain1: Entity representing the objects on the dimension 0 of the data matrix
   :param Entity domain2: Entity representing the objects on the dimension 1 of the data matrix

   :return: Returns 3 lists ``(x, y, corresp``) when used without side information. The first (x) is a list of tuples ``[(i,j),(k,j),...]`` containing the pairs of of objects belonging to domain1 and domain2 that needs to be predicted.
   The second (``y``) is a list containing the values of the input data matrix corresponding to the pairs of object in ``x``. 
   The third (corresp) is a list of tuples containing the corresponding names of the pairs of objects listed in ``x``.

.. py:function:: buildPytorchFeats(data:dict, domain1:Entity, domain2:Entity, side1 = None, side2 = None)

      This function is used to produce the input for the NNwrapper.predict() method, at prediction time. It produces the inputs necessary to predict the output for certain cells (or the entire matrix) for a given relation.

   This version of the method takes as prediction target a dict containing the pairs of ojects that need to be predicted. This is useful when only relatively few cells of the matrix need to be predicted (sparse prediction).

   :param dict data: Dict in the form {(obj[i],obj[j]):value1, (obj[i],obj[k]):value2, ...}. It represents the target cells of the matrix that need to be predicted. The actual values in the matrix are provided as value associated to each pair of objects in the dict, but are ignored during prediction. Used to represent sparse matrices.
   :param Entity domain1: Entity representing the objects on the dimension 0 of the data matrix
   :param Entity domain2: Entity representing the objects on the dimension 1 of the data matrix

   :return: Returns 3 lists ``(x, y, corresp)`` when used without side information. The first (x) is a list of tuples ``[(i,j),(k,j),...]`` containing the pairs of of objects belonging to domain1 and domain2 that needs to be predicted.
   The second (``y``) is a list containing the values of the input data matrix corresponding to the pairs of object in ``x``. 
   The third (``corresp``) is a list of tuples containing the corresponding names of the pairs of objects listed in ``x``.

.. py:function:: buildPytorchFeats(datam:DataMatrix, = None, side2 = None)

   This function is used to produce the input for the NNwrapper.predict() method, at prediction time. It produces the inputs necessary to predict the output for certain cells (or the entire matrix) for a given relation.
   This version of the method takes as prediction target a DataMatrix object.

   :param DataMatrix datam: DataMatrix object containing the matrix representing the predidction target. The actual values in the observed cells in the DataMatrix are provided as "labels" y in output, but are ignored during prediction.

   :return: Returns 3 lists ``(x, y, corresp)`` when used without side information. The first (x) is a list of tuples ``[(i,j),(k,j),...]`` containing the pairs of of objects belonging to domain1 and domain2 that needs to be predicted.
   The second (``y``) is a list containing the values of the input data matrix corresponding to the pairs of object in ``x``. 
   The third (``corresp``) is a list of tuples containing the corresponding names of the pairs of objects listed in x.

