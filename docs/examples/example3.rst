
Example3: Two relations among 3 entities
========================================

The ``examples/example3.py`` script shows how to use ``NXTfusion`` to perform inference over 3 :py:class:`NXTfusion.NXTfusion.Entity` connected by 2 :py:class:`NXTfusion.NXTfusion.Relation`.

As usual (see previous examples) we create the random matrices representing our relations.In this case we define also the protein-domain :py:class:`NXTfusion.NXTfusion.Relation`, creating a binary (0/1) matrix. The protein-domain :py:class:`NXTfusion.NXTfusion.Relation` mimicks the presence or absence of protein domains (e.g. PFAM) in each protein.

.. code-block:: python

   protEnt = NX.Entity("proteins", list(range(0,100)), np.int16)
   drugEnt = NX.Entity("compounds", list(range(0,1000)), np.int16)
   domainEnt = NX.Entity("protein", list(range(0,700)), np.int16)

   protDrugMat = np.random.rand(100, 1000)
   protDomainMat = np.random.randint(2, size=(100, 700))
   protDrugMat = DM.DataMatrix("protDrugMatrix", protEnt, drugEnt, protDrugMat)
   protDomainMat = DM.DataMatrix("protDomainMatrix", protEnt, domainEnt, protDomainMat)

We transformed the raw data in :py:class:`NXTfusion.DataMatrix.DataMatrix` objects, as usual.

We then define the losses. In this case, the protein-domain :py:class:`NXTfusion.NXTfusion.Relation` constitutes a binary prediction (discrimination) task, and so we use the ``t.nn.BCEWithLogitsLoss`` loss from pytorch and we specify "binary" as type for this loss. Ignore_index works as usual.

.. code-block:: python
  
   protDrugLoss = L.LossWrapper(t.nn.MSELoss(), type="regression", ignore_index = IGNORE_INDEX)
   protDomainLoss = L.LossWrapper(t.nn.BCEWithLogitsLoss(), type="binary", ignore_index = IGNORE_INDEX)

This time we will define two :py:class:`NXTfusion.NXTfusion.MetaRelation`, one for the prot-drug and one for the prot-domain relations. We append the corresponding :py:class:`NXTfusion.NXTfusion.Relation` to each MetaRelation.

   
Here we build the prot-drug MetaRelation:
.. code-block:: python

   protDrugRel = NX.MetaRelation("prot-drug", protEnt, drugEnt, None, None)
   protDrugRel.append(NX.Relation("drugInteraction", protEnt, drugEnt, protDrugMat, "regression", protDrugLoss, relationWeight=1))

And here we build the prot-domain :py:class:`NXTfusion.NXTfusion.Relation`. Finally, we add BOTH MetaRelations to the ERgraph.
   
.. code-block:: python

   protDomainRel = NX.MetaRelation("prot-domain", protEnt, domainEnt, None, None)
   protDomainRel.append(NX.Relation("pfamDomains", protEnt, domainEnt, protDomainMat, "binary", protDomainLoss, relationWeight=1))
   ERgraph = NX.ERgraph([protDrugRel, protDomainRel])

Using the NNwrapper object, we can perform training and testing as usual.

Please pay attention to the fact that ``BCEWithLogitsLoss`` does not use a Sigmoid activation in the NN. If, after prediction, you want to compute the prediction scores, you will have to apply ``t.nn.Sigmoid`` by yourself! (This is a ``pytorch`` good practice, not NXTfusion.)

