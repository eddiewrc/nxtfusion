NXTfusion.DataMatrix module
===========================

.. autofunction::NXTfusion.DataMatrix.DataMatrix
.. autodecorator::NXTfusion.DataMatrix.DataMatrix
.. autodata::NXTfusion.DataMatrix.DataMatrix
.. automethod::NXTfusion.DataMatrix.DataMatrix
.. autoattribute::NXTfusion.DataMatrix.DataMatrix


.. autoclass:: NXTfusion.DataMatrix.DataMatrix
   :members:
   :show-inheritance:


	.. py:method:: __init__(self, name:str, ent1:Entity, ent2:Entity, data:numpy.ndarray)

	   One of the alternative constructors for the DataMatrix class.
			
	   :param str name: Name of the data matrix

	   :param Entity ent1: Entity object representing the object on the dimension 0
	   :param Entity ent2: Entity object representing the object on the dimension 1
	   :param dict data:  Hash table containing the (sparse) elements and in the matrix describing the relation. The input "data" format should be: {(ent1, ent2): value} for all the observed elements in the matrix.
	   :param numpy.dtype dtype: The smallest possible type that could be used to store the elements of the matrix (e.g. np.int16 can represent up to 2^16 unique objects in the entity)

	   
      	.. py:method:: __init__(self, name:str, ent1:NX.Entity, ent2:NX.Entity, data:dict, dtype:type)

	   One of the alternative constructors for the DataMatrix class.
			
	   :param str name: Name of the data matrix

	   :param Entity ent1: Entity object representing the object on the dimension 0
	   :param Entity ent2: Entity object representing the object on the dimension 1
	   :param numpy.ndarray data: Numpy matrix containing the (dense) describing the relation between ent1 and en2. 
	   :param numpy.dtype dtype: The smallest possible type that could be used to store the elements of the matrix (e.g. np.int16)
	   :return: the message id

      	.. py:method:: __init__(self, name:str, data:numpy.ndarray, dtype:numpy.dtype)

	   Simplified constructor for the DataMatrix class. Entities are inferred from the dimensionality of the np.ndarray.
			
	   :param str name: Name of the data matrix

	   :param numpy.ndarray data: Numpy matrix containing the (dense) describing the relation between ent1 and en2. 
	   :param numpy.dtype dtype: The smallest possible type that could be used to store the elements of the matrix (e.g. np.int16 can represent up to 2^16 unique objects in the entity)

        .. py:method:: __init__(self, path:str)

	   Constructor that reads the DataMatrix from a previously serialized DataMatrix object.
			
	   :param str path: Path of the serialized DataMatrix

.. autoclass:: NXTfusion.DataMatrix.SideInfo
   :members:
   :undoc-members:
   :show-inheritance:  

      .. py:method:: __init__(self, name:str, ent1:Entity, ent2:Entity, data:dict)

	   One of the alternative constructors for the SideInfo class.
			
	   :param str name: Name of the data matrix
	   :param Entity ent1: Entity object representing the object on the dimension 0
	   :param dict data: Dict containing ent1 objects as keys and feature vectors (side information) as values.

           
      .. py:method:: __init__(self, name:str, ent1:Entity, ent2:Entity, data:numpy.ndarray)

	   One of the alternative constructors for the SideInfo class.
			
	   :param str name: Name of the data matrix
	   :param Entity ent1: Entity object representing the object on the dimension 0
	   :param numpy.ndarray data: Numpy array that contains the side information. It has shape (ent1 obj, feature length), similarly to a scikit-learn feature vector.

      .. py:method:: __init__(self, name:str, ent1:Entity, ent2:Entity, data:scipy.sparse.coo_matrix)

	   One of the alternative constructors for the SideInfo class.
			
	   :param str name: Name of the data matrix
	   :param Entity ent1: Entity object representing the object on the dimension 0
	   :param scipy.sparse.coo_matrix data: Scipy coo_matrix that contains the side information. It has shape (ent1 obj, feature length), similarly to a scikit-learn feature vector. It can be sparse, but currently the sparsity during mini batching is NOT supported.

      .. py:method:: __init__(self, path:str)

	   This constructor reads a serialized (SideInfo.dump()) SideInfo object.						
	   :param str path: Path to the serialized SideInfo object.

